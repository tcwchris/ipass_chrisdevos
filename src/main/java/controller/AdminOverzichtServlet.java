package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ServiceProvider;
import model.Values;
import model.ValuesService;

public class AdminOverzichtServlet extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			 throws ServletException, IOException {
		
		ValuesService waarde = ServiceProvider.getValueService();
		int wuid;
		int muid;
		
		if(req.getParameter("wuid") == null){
			wuid = 0;
		}else{
			wuid = Integer.parseInt(req.getParameter("wuid"));
		}
		if(req.getParameter("muid") == null){
			muid = 0;
		}else{
			muid = Integer.parseInt(req.getParameter("muid"));
		}
	
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		
		RequestDispatcher rd = null;
		
		if(wuid != 0){
			
			List<Values> waardes = waarde.findWeekPerson(ft.format(dNow),wuid);
			
			req.setAttribute("value_object", waardes);
			
			rd = req.getRequestDispatcher("/logged/admin/overzicht.jsp");
			
		} else if(muid != 0){
			
			List<Values> waardes = waarde.findMonthPerson(ft.format(dNow),muid);
			
			req.setAttribute("value_object", waardes);
			
			rd = req.getRequestDispatcher("/logged/admin/overzicht.jsp");
			
		}else{
			rd = req.getRequestDispatcher("/logged/admin/welcome.jsp");
			
		}
		rd.forward(req, resp);
 		
	}

}