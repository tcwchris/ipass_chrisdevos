package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ServiceProvider;
import model.User;
import model.UserService;
import model.Values;
import model.ValuesService;

public class LoginServlet extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			 throws ServletException, IOException {
		
		UserService service = ServiceProvider.getUserService();
		ValuesService waarde = ServiceProvider.getValueService();
		
		String name = req.getParameter("username");
		String pw = req.getParameter("password");
		User user = service.getUserByMail(name, pw);
		
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		
			if(user.getEmail() != ""){					
								
				RequestDispatcher rd = null;
				req.getSession().setAttribute("user_object", user);
				req.getSession().setAttribute("user_ID", user.getUserID());
				req.getSession().setAttribute("fName", user.getFirstname());
				req.getSession().setAttribute("lName", user.getLastname());
				req.getSession().setAttribute("mail", user.getEmail());
				req.getSession().setAttribute("uType", user.getUserType());
				
				
				/* 1 = gebruiker 
				 * 2 = verzorgende 
				 * */
				
				if(user.getUserType() == 1){
			
					
					
					List<Values> waardes = waarde.findWeekPerson(ft.format(dNow), user.getUserID());
					
					req.getSession().setAttribute("value_object", waardes);
					
					rd = req.getRequestDispatcher("/logged/user/welcome.jsp");	
				}else if(user.getUserType() == 2){
					
					List<Values>waardes = waarde.findDayAll(ft.format(dNow));
					
					req.getSession().setAttribute("value_object", waardes);
					rd = req.getRequestDispatcher("/logged/admin/welcome.jsp");
				}
				rd.forward(req, resp);
			}else{					
				RequestDispatcher rd = null;
				req.setAttribute("msgs", "inloggegevens zijn niet correct");
				rd = req.getRequestDispatcher("/index.jsp");
				rd.forward(req, resp);
			}
	}
}