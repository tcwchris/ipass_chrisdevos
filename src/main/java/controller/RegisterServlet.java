package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ServiceProvider;
import model.UserService;

public class RegisterServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			 throws ServletException, IOException {
		
		UserService service = ServiceProvider.getUserService();
		
		String mail 	= 	req.getParameter("email");
		String pw 		= 	req.getParameter("password");
		String fname 	= 	req.getParameter("fname");
		String lname 	= 	req.getParameter("lname");
		
		int userE = service.userExists(mail);
		
		
		if(userE == 0){
			RequestDispatcher rd = null;
			
			boolean userA = service.addUser(mail,pw,fname,lname);
			
			if(userA == true){
				req.setAttribute("msgs1", "uw account is succesvol aangemaakt. U kunt nu inloggen");
				rd = req.getRequestDispatcher("/index.jsp");
				rd.forward(req, resp);
				
			}
			else{
				req.setAttribute("msgs", "Gebruiker kon niet toegevoegd worden, probeer het aub later opnieuw");
				rd = req.getRequestDispatcher("/registreren.jsp");
				rd.forward(req, resp);
				
			}
			
		}else{
			RequestDispatcher rd = null;

			
			req.setAttribute("msgs", "Dit email is al in gebruik");
			rd = req.getRequestDispatcher("/registreren.jsp");
			rd.forward(req, resp);
			
		}

		
			}

}
