package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ServiceProvider;
import model.Values;
import model.ValuesService;
	
public class UserInvoerServlet extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		ValuesService waarde = ServiceProvider.getValueService();
			
		Object u = req.getSession().getAttribute("user_ID");
		int uid = (int)u;
	
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		RequestDispatcher rd = null;
		
		boolean exists = waarde.doesExist(ft.format(dNow), uid);
	    
	    if(exists ==  false){
	    	Boolean addUser = waarde.addValues(ft.format(dNow), uid);
	    	if(addUser == true){
		    	List<Values> waardes = waarde.findDayUser(ft.format(dNow), uid);
		 	    req.setAttribute("value_object", waardes);
		 		rd = req.getRequestDispatcher("/logged/user/invoer.jsp");
	    	}
	    }
	    else{
	    List<Values> waardes = waarde.findDayUser(ft.format(dNow), uid);
	    req.setAttribute("value_object", waardes);
		rd = req.getRequestDispatcher("/logged/user/invoer.jsp");
	    }
	    
	    rd.forward(req, resp);
	    
		
		}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		ValuesService waarde = ServiceProvider.getValueService();
		
		Object u = req.getSession().getAttribute("user_ID");
		int uid = (int)u;
	
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		RequestDispatcher rd = null;

		double oWaarde = Double.parseDouble(req.getParameter("owaarde"));
		int oUnits = Integer.parseInt(req.getParameter("ounit"));
		
		double mWaarde = Double.parseDouble(req.getParameter("mwaarde"));
		int mUnits = Integer.parseInt(req.getParameter("munit"));
		
		double aWaarde = Double.parseDouble(req.getParameter("awaarde"));
		int aUnits = Integer.parseInt(req.getParameter("aunit"));
		
		Boolean alterUser = waarde.alterValues(oWaarde, oUnits, mWaarde, mUnits, aWaarde, aUnits, ft.format(dNow), uid);
		
		req.setAttribute("values_alt", "Uw waardes zijn aangepast en verwerkt.");
		List<Values> waardes = waarde.findWeekPerson(ft.format(dNow), uid);
		req.getSession().setAttribute("value_object", waardes);
		rd = req.getRequestDispatcher("/logged/user/welcome.jsp");
		    
		rd.forward(req, resp);
		
	
	
	}
}
