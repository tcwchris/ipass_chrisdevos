package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ServiceProvider;
import model.Values;
import model.ValuesService;

public class UserOverzichtServlet extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			 throws ServletException, IOException {
		
		ValuesService waarde = ServiceProvider.getValueService();
		
		
		Object u = req.getSession().getAttribute("user_ID");
		int uid = (int)u;
	
		Date dNow = new Date();
	    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		
		RequestDispatcher rd = null;
		
			
		List<Values> waardes = waarde.findMonthPerson(ft.format(dNow),uid);
			
		req.setAttribute("value_object", waardes);
			
		rd = req.getRequestDispatcher("/logged/user/overzicht.jsp");
		rd.forward(req, resp);
			
 		
	}

}