package model;

public class ServiceProvider {
	
	private static UserService service = new UserService();
	private static ValuesService waarde = new ValuesService();
	
	public static UserService getUserService(){
		return service;
	}
	
	public static ValuesService getValueService(){
		return waarde;
	}

}