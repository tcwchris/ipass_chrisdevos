package model;

import java.util.ArrayList;

public class User {
	
	private String firstName;
	private String lastName;
	private String email;
	private int userType;
	private int userID;
	
	
	private static ArrayList<User> usersList = new ArrayList<User>();
	
	public User(String fNm, String lNm, String em, int uT, int uID){
		
		firstName = fNm;
		lastName = lNm;
		email = em;
		userType = uT;
		userID = uID;
	}

	public String getFirstname() { // getter voor attribuut naam
		 return firstName;
		}
	public String getLastname() { // getter voor attribuut naam
		 return lastName;
		}
	public String getEmail() { // getter voor attribuut naam
		 return email;
		}
	public int getUserType() { // getter voor attribuut naam
		 return userType;
		}
	public int getUserID(){
		return userID;
	}
	
	public ArrayList<User> getUser(){
		return User.usersList;
	}	
}