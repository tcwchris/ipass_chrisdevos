package model;

import java.util.List;

import persistence.UserDAO;

public class UserService {
	
	private UserDAO userDAO = new UserDAO();
	
	public List<User> findAllUsers() {
		return userDAO.findAll();
	}
	
	public User getUserByMail(String email, String password) {
		return userDAO.findByUser(email, password);
	}
	
	public int userExists(String email){
		return userDAO.userExists(email);
	}
	public boolean addUser(String mail, String pw, String fname, String lname){
		return userDAO.userAdd(mail,pw,fname,lname);
	}
}