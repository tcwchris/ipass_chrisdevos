package model;

import java.util.ArrayList;
import java.util.List;

public class Values {
	
	private static List<Values> valueList = new ArrayList<Values>();
	
	double	oWaarde;
	int 	oUnits;
	double	mWaarde;
	int		mUnits;
	double	aWaarde;
	int		aUnits;
	String	date;
	String 	firstname;
	String 	lastname;
	int		userID;
	
	public Values(double oWd, int oUn,double mWd, int mUn,double aWd, int aUn,String datum){
		
		oWaarde	=	oWd;
		oUnits	=	oUn;
		mWaarde	=	mWd;
		mUnits	=	mUn;
		aWaarde	=	aWd;
		aUnits	=	aUn;
		date	=	datum;
		firstname =	" ";
		lastname = 	" ";
		userID	=	0;
	}
	
public Values(double oWd, int oUn,double mWd, int mUn,double aWd, int aUn,String datum,String fnm, String lnm, int uid){
		
		oWaarde	=	oWd;
		oUnits	=	oUn;
		mWaarde	=	mWd;
		mUnits	=	mUn;
		aWaarde	=	aWd;
		aUnits	=	aUn;
		date	=	datum;
		firstname = fnm;
		lastname = 	lnm;
		userID	=	uid;
	}

	public double getOWaarde() { // getter voor attribuut naam
		 return oWaarde;
		}
	public int getOUnits() { // getter voor attribuut naam
		 return oUnits;
		}
	public double getMWaarde() { // getter voor attribuut naam
		 return mWaarde;
		}
	public int getMUnits() { // getter voor attribuut naam
		 return mUnits;
		}
	public double getAWaarde() { // getter voor attribuut naam
		 return aWaarde;
		}
	public int getAUnits() { // getter voor attribuut naam
		 return aUnits;
		}
	public int getUid() { // getter voor attribuut naam
		 return userID;
	}
	public String getDate() { // getter voor attribuut naam
		 return date;
		}
	
	public String getFirstname() { // getter voor attribuut naam
		 return firstname;
		}
	public String getLastname() { // getter voor attribuut naam
		 return lastname;
		}
	public List<Values> getValues(){
		return Values.valueList;
		}	
}