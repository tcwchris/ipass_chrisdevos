package model;

import java.util.List;

import persistence.WaardeDAO;

public class ValuesService {
	
	private WaardeDAO valueDAO = new WaardeDAO();
	
	public List<Values> findAll(){
		return valueDAO.findAll();
	}
	
	public boolean doesExist(String datum, int uid){
		return valueDAO.doesExist(datum, uid);
	}
	public boolean addValues(String datum, int uid){
		return valueDAO.addValues(datum, uid);
	}
	
	public boolean alterValues(double oWaarde, int oUnits, double mWaarde,int mUnits,double aWaarde,int aUnits,String datum, int uid){
		return valueDAO.alterValues(oWaarde, oUnits, mWaarde, mUnits, aWaarde, aUnits,datum,uid);
	}
	
	public List<Values> findDayUser(String datum, int uid){
		return valueDAO.findDayUser(datum, uid);
	}
	
	public List<Values> findDayAll(String datum){
		return valueDAO.findDayAll(datum);
	}
	
	public List<Values> findWeekPerson(String datum, int uid){
		return valueDAO.findWeekPerson(datum,uid);
	}
	public List<Values> findMonthPerson(String datum, int uid){
		return valueDAO.findMonthPerson(datum, uid);
	}
}