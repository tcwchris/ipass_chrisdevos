package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDAO extends BaseDAO{
	
	private List<User> selectUsers(String query){
		List<User> results = new ArrayList<User>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
				String firstName = dbResultSet.getString("firstname");
				String lastName = dbResultSet.getString("lastname");
				String email = dbResultSet.getString("email");
				int uType = dbResultSet.getInt("utype");
				int uID = dbResultSet.getInt("uid");
			
				User newUser = new User(firstName,lastName,email,uType,uID);	
				results.add(newUser);
				}
			
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
	return results;
	}
	
	private User checkLogin(String query){
		List<User> results = new ArrayList<User>();
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
				String firstName = dbResultSet.getString("firstname");
				String lastName = dbResultSet.getString("lastname");
				String email = dbResultSet.getString("email");
				int uType = dbResultSet.getInt("utype");
				int uID = dbResultSet.getInt("uid");
			
				User newUser = new User(firstName,lastName,email,uType,uID);	
				results.add(newUser);
				}
		}
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
		if(results.isEmpty()){
			User newUser = new User("","","",0,0);	
			results.add(newUser);
		}
		
		return results.get(0);
	}
	
	private int countUsers(String query){
		int results = 0;
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
			results = dbResultSet.getInt("totaal");
			}
			
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
	return results;
	}
	
	private boolean addUsers(String query){
		boolean results = false;
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
			results = true;
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
	return results;
	}
	
	
	public List<User> findAll(){
		return selectUsers("SELECT * FROM users");
	}
	
	public User findByUser(String email, String password){
		return checkLogin("SELECT * FROM users WHERE email = '" + email + "' AND password = '" + password + "' ");
	}
	
	public int userExists(String email){
		return countUsers("SELECT COUNT(*) AS totaal FROM users WHERE email = '"+ email +"'");
	}
	public boolean userAdd(String mail, String pw, String fname, String lname){
		return addUsers("INSERT INTO users(firstname, lastname, email,password,utype) VALUES ('"+ fname +"' , '"+ lname +"' , '"+ mail +"' , '"+ pw +"',1)");
	}
	

}
