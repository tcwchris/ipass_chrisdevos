package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Values;

public class WaardeDAO extends BaseDAO {
	
	private List<Values> selectWaardes(String query){
		List<Values> results = new ArrayList<Values>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
				double oWa = dbResultSet.getDouble("owaarde");
				int oUn = dbResultSet.getInt("ounits");
				
				double mWa = dbResultSet.getDouble("mwaarde");
				int mUn = dbResultSet.getInt("munits");
				
				double aWa = dbResultSet.getDouble("awaarde");
				int aUn = dbResultSet.getInt("aunits");
				
				String datum = dbResultSet.getString("datum");
				
				Values newValue = new Values(oWa,oUn,mWa,mUn,aWa,aUn,datum);
				
				results.add(newValue);
				}
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
	return results;
	}
	
	
	private List<Values> selectWaardesExtra(String query){
		List<Values> results = new ArrayList<Values>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
				double oWa = dbResultSet.getDouble("owaarde");
				int oUn = dbResultSet.getInt("ounits");
				
				double mWa = dbResultSet.getDouble("mwaarde");
				int mUn = dbResultSet.getInt("munits");
				
				double aWa = dbResultSet.getDouble("awaarde");
				int aUn = dbResultSet.getInt("aunits");
				
				String datum = dbResultSet.getString("datum");
				
				
					String fnm = dbResultSet.getString("firstname");
					String lnm = dbResultSet.getString("lastname");
					int uid = dbResultSet.getInt("uid");
					
					Values newValue = new Values(oWa,oUn,mWa,mUn,aWa,aUn,datum,fnm,lnm,uid);
					
					results.add(newValue);	
				}
			
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
	return results;
	}
	
	
	private boolean selectExist(String query){
		boolean results = false;
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while(dbResultSet.next()){
				int bestaat = dbResultSet.getInt("bestaat");
				
				if(bestaat == 0){
					results = false;
					
				}else{
					results = true;
				}
				
				}
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
	return results;
	}
	
	private boolean addalterValuesrow(String query){
		boolean results = false;
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
			results = true;
		}	
		catch(SQLException sqle){
			sqle.printStackTrace();
		}
	return results;
	}
	
	public List<Values> findAll(){
		return selectWaardes("SELECT * FROM waardes");
	}
	public List<Values> findDayUser(String datum, int uid){
		return selectWaardes("SELECT* FROM waardes WHERE datum = '"+ datum +"' AND uid = "+ uid +"  ");
	}
	
	public boolean doesExist(String datum, int uid){
		return selectExist("SELECT COUNT(*) AS bestaat FROM waardes WHERE datum = '"+ datum +"' AND uid = "+ uid +"  ");
	}
	public boolean addValues(String datum, int uid){
		return addalterValuesrow("INSERT INTO waardes(owaarde,ounits,mwaarde,munits,awaarde,aunits,datum,uid)VALUES(0.0,0,0.0,0,0.0,0,'"+datum+"',"+ uid +")");
	}
	public boolean alterValues(double oWaarde, int oUnits, double mWaarde,int mUnits,double aWaarde,int aUnits,String datum, int uid){
		return addalterValuesrow("UPDATE waardes SET owaarde = "+oWaarde+", ounits = "+oUnits+", mwaarde = "+mWaarde+",munits = "+mUnits+",awaarde = "+aWaarde+",aunits = "+aUnits+" WHERE uid = "+uid+" AND datum = '"+datum+"' ");
	}
	
	public List<Values> findDayAll(String datum){
		return selectWaardesExtra("SELECT waardes.*, users.firstname , users.lastname FROM waardes LEFT JOIN users ON waardes.uid=users.uID WHERE datum = '"+ datum +"'  ORDER BY datum ");
	}
	public List<Values> findWeekPerson(String datum, int uid){
		return selectWaardes("SELECT * FROM waardes WHERE Week(datum) = Week('"+ datum +"')AND uid = " + uid+" ORDER BY datum");
	}
	public List<Values> findMonthPerson(String datum, int uid){
		return selectWaardes("SELECT * FROM waardes WHERE Month(datum) = Month('"+ datum +"')AND uid = '"+ uid +"' ORDER BY datum");
	}

}
