<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Suikerwaarde </title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css" href="/css/login.css">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="model.ServiceProvider" %>
<% request.setAttribute("service", ServiceProvider.getUserService()); %>

</head>
<body>
<%
Object msgs = request.getAttribute("msgs");
Object msgs1 = request.getAttribute("msgs1");
%>
  <div class="wrapper">
  	<% if (msgs != null) { %>
  	<div class="alert alert-danger" role="alert"><% out.println(msgs); %></div>
  	<%} 
  	
  	if (msgs1 != null) { %>
  	<div class="alert alert-success" role="alert"><% out.println(msgs1); %></div>
  	<%} %>
  
    <form class="form-signin" action="LoginServlet.do" method="post">       
      <h2 class="form-signin-heading">Loginscherm<br> SB-systeem</h2>
      <input type="text" class="form-control" name="username" placeholder="Uw email" required  /><br>
      <input type="password" class="form-control" name="password" placeholder="Uw paswoord" required/><br>      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button><br>
      <a href="/registreren.jsp">Nog geen account? Registreer hier!</a>   
    </form>
    
    
    
  </div>

</body>
</html>
