<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
    <title>Simple Sidebar - Start Bootstrap Template</title>
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	

    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="/logged/user/welcome.jsp">index</a>
                </li>
                <li>
                    <a href="/logged/user/UserInvoerServlet.do">Gegevens invoeren</a>
                </li>
                <li>
                    <a href="/logged/user/UserOverzichtServlet.do">Maandoverzicht</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        
                        <table>

						    <tbody>
						    	<form method="post" action="UserInvoerServlet.do">
								<c:forEach items="${requestScope.value_object}" var="waarde">
							    	<tr>
							    	
							    		<td>Datum:<td>${waarde.date}</td>
							    	</tr>
							    	<tr>
							    		<td>Ochtendwaarde:</td>	<td><input type="text" class="form-control" name="owaarde" value="${waarde.OWaarde}"></td>
							    	</tr>
							    	<tr>
							    		<td>Ochtend Units:</td><td><input type="text" class="form-control" name="ounit" value="${waarde.OUnits}"></td>
							    	</tr>
							    	<tr>
							    		<td>Avondwaarde: </td><td><input type="text" class="form-control" name="mwaarde" value="${waarde.MWaarde}"></td>
							    	</tr>
							    	<tr>
							    		<td>Avond Units:</td> <td><input type="text" class="form-control" name="munit" value="${waarde.MUnits}"></td>
							    	</tr>
							    	<tr>
							    		<td>Avondwaarde:</td> <td><input type="text" class="form-control" name="awaarde" value="${waarde.AWaarde}"></td>
							    	</tr>
							    	<tr>
							    		<td>Avond Units:</td> <td><input type="text" class="form-control" name="aunit" value="${waarde.AUnits}"></td>		
							    	</tr>
								</c:forEach>
									
									<tr><td></td><td><input class="btn btn-default" type="submit" value="Gegevens bijwerken"></td></tr>
								</form>
						    </tbody>
						  </table>
         			</div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>

</html>
