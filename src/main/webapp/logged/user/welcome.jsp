<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
    <title>Simple Sidebar - Start Bootstrap Template</title>
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	

    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="/logged/user/welcome.jsp">index</a>
                </li>
                <li>
                    <a href="/logged/user/UserInvoerServlet.do">Gegevens invoeren</a>
                </li>
                <li>
                    <a href="/logged/user/UserOverzichtServlet.do">Maandoverzicht</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                
                	
                    <div class="col-lg-12">
                    	<%
                    	Object alt = request.getAttribute("values_alt");
  						if (alt != null) { %>
  						<div class="alert alert-success" role="alert"><% out.println(alt); %></div>
  						<%} %>
                       <h1> Welkom <%
							Object name = session.getAttribute("fName");
							if (name != null) {
							out.println(name);
							}
							%></h1>
                        <p>Deze pagina krijg je alleen maar te zien als je ingelogd bent als gebruiker</p>
                        <p>Dit betekent dus dat jij bent ingelogd als gebruiker. <code>#Swag</code>.</p>
                        
                        <br><br><br>
                        
                        <table class="table table-striped">
						    <thead>
						      <tr> 
						        <th>Datum</th>
						        <th>Ochtend waarde</th>
						        <th>Ochtend units</th>
						        <th>Middag waarde</th>
						        <th>Middag units</th>
						        <th>Avond waarde</th>
						        <th>Avond units</th>
						      </tr>
						    </thead>
						    <tbody>
								<c:forEach items="${sessionScope.value_object}" var="waarde">
							    	<tr>
							    		<td>${waarde.date}</td>
							    		<td>${waarde.OWaarde}</td>
							    		<td>${waarde.OUnits}</td>
							    		<td>${waarde.MWaarde}</td>
							    		<td>${waarde.MUnits}</td>
							    		<td>${waarde.AWaarde}</td>
							    		<td>${waarde.AUnits}</td>		
							    	</tr>
								</c:forEach>
						    </tbody>
						  </table>
                       </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>

</html>
